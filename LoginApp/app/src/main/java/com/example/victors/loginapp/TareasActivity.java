package com.example.victors.loginapp;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TareasActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener{

    private LinearLayout Prof_Section;
    private Button SignOut;
    private TextView Name, Email;
    private ImageView Prof_pic;
    private GoogleApiClient googleApiClient;
    private static final int REQ_CODE = 9001;

    private RecuperarTareas recuperarTareas = null;
    //private RecuperarVacunaciones recuperarVacunaciones = null;
    private String resultado = "";
    private int pUsuarioId = 0;
    private int pTareaId = 0;
    private TableLayout tablaTareas;
    JSONArray jsonArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tareas);

        SignOut = (Button)findViewById(R.id.btn_logout_);
        Name = (TextView) findViewById(R.id.name_);
        Email = (TextView) findViewById(R.id.email_);
        Prof_pic = (ImageView)findViewById(R.id.prof_pic_);
        SignOut.setOnClickListener(this);
        Bundle datos = this.getIntent().getExtras();
        Name.setText(datos.getString("name"));
        Email.setText(datos.getString("email"));
        pUsuarioId = datos.getInt("usuarioId");
        Glide.with(this).load(datos.getString("foto")).into(Prof_pic);
        GoogleSignInOptions signInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this,this).addApi(Auth.GOOGLE_SIGN_IN_API,signInOptions).build();


        tablaTareas = (TableLayout) findViewById(R.id.tabla_tareas);
        recuperarTareas = new RecuperarTareas();
        recuperarTareas.execute();

    }

    protected void cargarTablaActividades(){
        if(tablaTareas.getChildCount() > 1){
            int filas = tablaTareas.getChildCount();

            tablaTareas.removeViews(1, filas-1);
        }

        Integer count=0;
        // Create the table row
        if(jsonArray.length() != 0){
            for (int i=0; i<jsonArray.length(); i++){
                try {
                    JSONObject json = jsonArray.getJSONObject(i);

                    TableRow tr = new TableRow(this);
                    //if(count%2!=0){ tr.setBackgroundColor(Color.WHITE);}else{tr.setBackgroundColor(Color.GREEN);};
                    tr.setId(100+count);
                    tr.setLayoutParams(new TableLayout.LayoutParams(
                            TableRow.LayoutParams.FILL_PARENT,
                            TableRow.LayoutParams.WRAP_CONTENT));
                    tr.setOnClickListener( new View.OnClickListener() {
                        @Override
                        public void onClick( View v ) {
                            TableRow t = (TableRow) v;
                            TextView firstTextView = (TextView) t.getChildAt(0);
                            TextView secondTextView = (TextView) t.getChildAt(1);
                            TextView thirdTextView = (TextView) t.getChildAt(2);
                            TextView forthTextView = (TextView) t.getChildAt(3);
                            Integer firstText = Integer.parseInt(firstTextView.getText().toString());
                            String secondText = secondTextView.getText().toString();
                            String thirdText = thirdTextView.getText().toString();
                            String forthText = forthTextView.getText().toString();
                            //Toast.makeText(getApplicationContext() , "clic en row "+ firstText, Toast.LENGTH_LONG).show();

                            /*Intent in = new Intent(HijosActivity.this,VacunasActivity.class);
                            int idHijo = 0;
                            in.putExtra("hijoId", firstText);
                            in.putExtra("nombre", thirdText + " " + forthText);
                            in.putExtra("cedula", secondText);
                            startActivity(in);*/
                        }
                    } );
                    //Create columns to add as table data
                    TextView textId = new TextView(this);
                    textId.setId(200+count);
                    textId.setText(json.getString("idActividad"));
                    textId.setPadding(2, 0, 5, 0);
                    textId.setTextColor(Color.BLACK);
                    tr.addView(textId);

                    TextView textRequerimiento = new TextView(this);
                    textRequerimiento.setId(200+count);
                    textRequerimiento.setText(json.getString("idRequerimiento"));
                    textRequerimiento.setPadding(2, 0, 5, 0);
                    textRequerimiento.setTextColor(Color.BLACK);
                    tr.addView(textRequerimiento);

                    TextView textProyecto = new TextView(this);
                    textProyecto.setId(200+count);
                    textProyecto.setText(json.getString("idProyecto"));
                    textProyecto.setPadding(2, 0, 5, 0);
                    textProyecto.setTextColor(Color.BLACK);
                    tr.addView(textProyecto);

                    TextView textSprint = new TextView(this);
                    textSprint.setId(200+count);
                    textSprint.setText(json.getString("idSprint"));
                    textSprint.setPadding(2, 0, 5, 0);
                    textSprint.setTextColor(Color.BLACK);
                    tr.addView(textSprint);


                    // finally add this to the table row
                    tablaTareas.addView(tr, new TableLayout.LayoutParams(
                            TableRow.LayoutParams.FILL_PARENT,
                            TableRow.LayoutParams.WRAP_CONTENT));
                    count++;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }else{
            Toast.makeText(getApplicationContext() , "No tiene Actividades.", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onClick(View view) {
        new LoginActivity().signOut();
    }


    public void signOut(){
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                Intent i = new Intent(TareasActivity.this,LoginActivity.class);
                startActivity(i);
            }
        });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void mostrarNotificacion() {
        if(jsonArray.length() != 0){
            for (int i=0; i<jsonArray.length(); i++){
                try {
                    JSONObject json = jsonArray.getJSONObject(i);
                    pTareaId = Integer.parseInt(json.getString("idTarea"));
                    //recuperarVacunaciones = new RecuperarVacunaciones();
                    //recuperarVacunaciones.execute();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void initChannels(Context context) {
        if (Build.VERSION.SDK_INT < 26) {
            return;
        }
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationChannel channel = new NotificationChannel("default",
                "Channel name",
                NotificationManager.IMPORTANCE_DEFAULT);
        channel.setDescription("Channel description");
        notificationManager.createNotificationChannel(channel);
    }


    private class RecuperarTareas extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost post = new HttpPost("http://192.168.1.203:8083/backendProj/services/actividades/usuarios");
            post.setHeader("content-type", "application/json");
            try {
                JSONObject dato = new JSONObject();
                dato.put("id",pUsuarioId);
                StringEntity entity = new StringEntity(dato.toString());
                post.setEntity(entity);
                HttpResponse resp = httpClient.execute(post);
                resultado = EntityUtils.toString(resp.getEntity());
            }catch (Exception ex){
                ex.printStackTrace();
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if(!success){
                Toast.makeText(getApplicationContext() , "Error: no se recuperaron tareas para este usuario", Toast.LENGTH_LONG).show();
                //cargarTablaActividades();
            }
            else{
                try {
                    jsonArray = new JSONArray(resultado);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                cargarTablaActividades();
                mostrarNotificacion();
            }
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
        }
    }


}
