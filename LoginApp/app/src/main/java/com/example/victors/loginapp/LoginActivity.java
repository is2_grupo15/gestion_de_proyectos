package com.example.victors.loginapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;


import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;



import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;

import javax.net.ssl.HttpsURLConnection;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener,
        GoogleApiClient.OnConnectionFailedListener{

    private TextView mTextMessage;
    private GoogleSignInClient mGoogleSignInClient;
    private TextView mStatusTextView;
    private static final String TAG = "SignInActivity";
    private static final int REQ_CODE  = 9001;
    private SignInButton botonLogin;
    private Button botonSignOut;
    private GoogleApiClient googleApiClient;
    private LinearLayout Prof_Section;
    private TextView Name, Email;
    private ImageView Prof_pic;
    private String pEmail = "";
    private String resultado = "";
    private ValidarUsuario validarUsuario = null;
    private GoogleSignInAccount account;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    mTextMessage.setText(R.string.title_home);
                    return true;
                case R.id.navigation_dashboard:
                    mTextMessage.setText(R.string.title_dashboard);
                    return true;
                case R.id.navigation_notifications:
                    mTextMessage.setText(R.string.title_notifications);
                    return true;
            }
            return false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Views
        Prof_Section = findViewById(R.id.prof_section);
        botonLogin = findViewById(R.id.sign_in_button);
        botonLogin.setOnClickListener(this);
        //findViewById(R.id.button_sign_out).setOnClickListener(this);
        botonSignOut = findViewById(R.id.button_sign_out);
        botonSignOut.setOnClickListener(this);
        Name =  findViewById(R.id.name);
        Email =  findViewById(R.id.email);
        Prof_pic = findViewById(R.id.prof_pic);
        Prof_Section.setVisibility(View.GONE);

        mTextMessage = (TextView) findViewById(R.id.message);
        System.out.println("........0.........");
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        // Build a GoogleSignInClient with the options specified by gso.
       // mGoogleSignInClient  = GoogleSignIn.getClient(this, gso);
        //GoogleSignInOptions signInOptions = new GoogleSignInOptions.
          //      Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        //googleApiClient = new GoogleApiClient.Builder(this).
          //      enableAutoManage(this,this).
            //    addApi(Auth.GOOGLE_SIGN_IN_API, signInOptions).build();
        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this,this).addApi(Auth.GOOGLE_SIGN_IN_API, gso).build();
        System.out.println("........lalala.........");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    //@Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                signIn();
                break;
            case R.id.button_sign_out:
                signOut();
                break;
            // ...
        }
    }

    /*@Override
    public void onStart() {
        super.onStart();
        //email = findViewById(R.id.email);
       // Name  = findViewById(R.id.name);
        // [START on_start_sign_in]
        // Check for existing Google Sign In account, if the user is already signed in
        // the GoogleSignInAccount will be non-null.
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        updateUI(account);
        if (account != null) {
            String personName = account.getDisplayName();
            String personGivenName = account.getGivenName();
            String personFamilyName = account.getFamilyName();
            String personEmail = account.getEmail();
            String personId = account.getId();
            Uri personPhoto = account.getPhotoUrl();
            Email = findViewById(R.id.email);
            Name = findViewById(R.id.name);
            Name.setText(personName);
            Email.setText(personEmail);
            pEmail = personEmail;
            System.out.println("Captian America " + pEmail);
            validarUsuario = new ValidarUsuario();
            validarUsuario.execute();

        }
         //[END on_start_sign_in]
    }*/


    // [START revokeAccess]
    private void revokeAccess() {
        mGoogleSignInClient.revokeAccess()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // [START_EXCLUDE]
                        updateUI(false);
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END revokeAccess]

    /*private void updateUI(@Nullable GoogleSignInAccount account) {
        if (account != null) {
           // mStatusTextView.setText(getString(R.string.signed_in_fmt, account.getDisplayName()));

            findViewById(R.id.sign_in_button).setVisibility(View.GONE);
            findViewById(R.id.button_sign_out).setVisibility(View.VISIBLE);
        } else {
            //mStatusTextView.setText(R.string.logout);

            findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
            //findViewById(R.id.name).setVisibility(View.GONE);
            //findViewById(R.id.email).setVisibility(View.GONE);
            findViewById(R.id.button_sign_out).setVisibility(View.GONE);

        }
    }*/


    /*private class ValidarUsuario extends AsyncTask<Void, Void, Boolean> {

        @SuppressLint("NewApi")
        @Override
        protected Boolean doInBackground(Void... voids) {
            // Create URL
            URL url = null;
            try {
                url = new URL("http://192.168.1.203:8083/backendProj/services/usuarios");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            // Create connection
            HttpURLConnection myConnection = null;
            try {
                assert url != null;
                myConnection = (HttpURLConnection) Objects.requireNonNull(url).openConnection();
            } catch (IOException e) {
                e.printStackTrace();
            }

            myConnection.setRequestProperty("User-Agent", "my-rest-app-v0.1");
            myConnection.setRequestProperty("Accept",
                    "application/vnd.github.v3+json");
            myConnection.setRequestProperty("Contact-Me",
                    "soporte@gmail.com");


            try {
                if (myConnection.getResponseCode() == 200) {
                    // Success
                    // Further processing here
                } else {
                    // Error handling code goes here
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                JSONObject dato = new JSONObject();
                dato.put("email",pEmail);

            }catch (Exception ex){
                ex.printStackTrace();
                signOut();
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if(!success){
                Toast.makeText(getApplicationContext() , "Error: no se encuentra el usuario registrado en el sistema", Toast.LENGTH_LONG).show();
                updateUI(null);
            }
            else{

                updateUI(null);
            }
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
        }
    }*/



    public void signIn() {
        System.out.println("........0.5.........");
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, REQ_CODE);
        //startActivityForResult(signInIntent, RC_SIGN_IN);




    }


    public void signOut() {
        //mGoogleSignInClient.signOut()
               // .addOnCompleteListener(this, new OnCompleteListener<Void>() {
               //     @Override
                 //   public void onComplete(@NonNull Task<Void> task) {
                 //       // ...
               //     }
             //   });
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                updateUI(false);
            }
        });
    }



    /*public void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }

        if (result.isSuccess()){
            account = result.getSignInAccount();
            String name = account.getDisplayName();
            String email = account.getEmail();
            String imgUrl = account.getPhotoUrl().toString();
            Name.setText(name);
            Email.setText(email);
            Glide.with(this).load(imgUrl).into(Prof_pic);
            pEmail = email;
            System.out.println("Captian America " + pEmail);
            validarUsuario = new ValidarUsuario();
            validarUsuario.execute();
        }else{
            updateUI(false);
        }


    }*/

    public void handleSignInResult(GoogleSignInResult result){

        if (result.isSuccess()){
            account = result.getSignInAccount();
            String name = account.getDisplayName();
            String email = account.getEmail();
            if(account.getPhotoUrl() != null){
                String imgUrl = account.getPhotoUrl().toString();
                Glide.with(this).load(imgUrl).into(Prof_pic);
            }
            Name.setText(name);
            Email.setText(email);
            pEmail = email;
            System.out.println("Captian America " + pEmail);
            validarUsuario = new ValidarUsuario();
            validarUsuario.execute();
        }else{
            updateUI(false);
        }
    }

    public void updateUI(boolean isLogin){
        if (isLogin){
            Intent i = new Intent(LoginActivity.this,TareasActivity.class);
            int iduser = 0;
            int id = 0;
            try {
                JSONObject userJson = new JSONObject(resultado);
                //iduser = userJson.getInt("idUsuario");
                //System.out.println("Mandiiiiiiiiiiiii"+iduser);
                id = userJson.getInt("id");
                //System.out.println("Andreeeeeeessss"+id);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            i.putExtra("email", account.getEmail());
            i.putExtra("name", account.getDisplayName());
            if(account.getPhotoUrl() != null){
                i.putExtra("foto", account.getPhotoUrl().toString());
            }
            i.putExtra("usuarioId", id);
            startActivity(i);
            Prof_Section.setVisibility(View.VISIBLE);
            botonLogin.setVisibility(View.GONE);
        }else{
            Prof_Section.setVisibility(View.GONE);
            botonLogin.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == REQ_CODE) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            /*Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);*/
            System.out.println(".......1..........");
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private class ValidarUsuario extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            System.out.println("........3.........");
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost post = new HttpPost("http://192.168.1.203:8083/backendProj/services/usuarios/validarUsuario");
            post.setHeader("content-type", "application/json");
            try {
                JSONObject dato = new JSONObject();
                dato.put("email",pEmail);
                StringEntity entity = new StringEntity(dato.toString());
                post.setEntity(entity);
                HttpResponse resp = httpClient.execute(post);
                if(resp.getStatusLine().getStatusCode() == HttpURLConnection.HTTP_OK) {
                    resultado = EntityUtils.toString(resp.getEntity());
                }else{
                    signOut();
                    return false;
                }
            }catch (Exception ex){
                ex.printStackTrace();
                signOut();
                return false;
            }
            return true;
        }


        @Override
        protected void onPostExecute(Boolean success) {
            if(!success){
                Toast.makeText(getApplicationContext() , "Error: no se encuentra el usuario registrado en el sistema", Toast.LENGTH_LONG).show();
                updateUI(false);
            }
            else{
                System.out.println("Holaaaaaaa");
                updateUI(true);
            }
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
        }
    }
    /*    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }*/




}
